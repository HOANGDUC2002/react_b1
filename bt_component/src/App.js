import "./App.css";
import NavTop from "./Component/NavTop";
import Header from "./Component/Header";
import Banner from "./Component/Banner";
import Footer from "./Component/Footer";

function App() {
  return (
    <div className="App">
      <NavTop />
      <Header />
      <Banner />
      <Footer />
    </div>
  );
}

export default App;
